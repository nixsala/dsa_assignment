package qstackqueueparli.ni;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BothStackQueue {

	public static boolean is_palindrome(String input) {
		Queue<Character> q = new LinkedList<Character>();
		Stack<Character> s = new Stack<Character>();
		Character letter;
		int mismatches = 0;

		for (int i = 0; i < input.length(); i++) {
			letter = input.charAt(i);
			if (Character.isLetter(letter)) {
				q.add(letter);
				s.push(letter);
			}
		}
		System.out.println("test");
		while (!q.isEmpty()) {
			if (q.remove() != s.pop())
				mismatches++;
		}
		
		return (mismatches == 0);
		
}
}